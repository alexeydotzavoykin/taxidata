An example of setting up Spring-Boot with Spark.

This application was designed by Alexey Zavoykin, Alexandr Goncharov and Natalia Novikova to participate in internal
hackathon runned by Akvelon and Dday hackathon in 2017. Both tournament were dedicated to Big Data solutions.

Using this app could help you during work with New Your taxi dataset (as we tested and used it), but with some minor
changes it could be used to work with any dataset, especially if it is presented in csv format.
Think twice, before declining ideas of using SQL!

Setting Up:

1. Download version of Spark from https://spark.apache.org/downloads.html
2. Put it in C:Spark/ directory 
3. Check that final path and version is exactly the same as the one mentioned in resource file. 

Important issue! 
If you get "Out of memory" exception: 
1. In your IDE go to Run configuration > Arguments > VM Arguments 
2. Set max heapsize like -Xmx512m 


