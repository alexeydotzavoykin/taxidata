package com.myteam.taxidata.model;

import org.apache.spark.sql.Row;

public class KeyValueData {

    private String key;
    private String value;

    public KeyValueData(Row row) {
        if (row.size()<2){
            this.key =  "0";
            this.value =  String.valueOf(row.get(0));
        }
        else {
            this.key = String.valueOf(row.get(0));
            this.value = String.valueOf(row.get(1));
        }
    }

    public KeyValueData(String key, String value) {
            this.key =  key;
            this.value =  value;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
