package com.myteam.taxidata.counter;

import com.myteam.taxidata.model.KeyValueData;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.regression.LinearRegressionWithSGD;
import org.apache.spark.sql.*;
import org.springframework.stereotype.Component;

import java.util.*;

import static org.apache.spark.sql.functions.*;

import org.apache.spark.mllib.linalg.*;
import org.apache.spark.mllib.stat.Statistics;

@Component
public class TaxiCounter extends AbstractCounter{

    public static Function<Row, LabeledPoint> mapperFunc = new Function<Row, LabeledPoint>() {
        @Override
        public LabeledPoint call(Row row) {
            String lineAsString = row.toString().replaceAll("[\\[\\]]", "");
            String[] fields = lineAsString.split(",");
            double[] featureValues = new double[fields.length-1];
            for (int i = 1; i < fields.length; i++) {
                featureValues[i-1] = Double.parseDouble(fields[i]);
            }
            //ArrayList<Double> list = new ArrayList<>(Arrays.asList(v));
            //Double labelData = v[v.length - 1];
            //return new LabeledPoint(labelData, Vectors.dense(v));

            LabeledPoint labeledPoint;
            labeledPoint = new LabeledPoint(
                    Double.valueOf(fields[0]),
                    Vectors.dense(featureValues));
            return labeledPoint;
        }
    };

    public List<KeyValueData> count(String columnName) {
        List<Row> rows = taxiDataService.getDataset()
                .groupBy(col(columnName))
                .count()
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> geoDataForMap(String xColumn, String yColumn) {
        List<Row> rows = taxiDataService.getDataset()
                .select(xColumn, yColumn)
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> avgByFields(String xColumn, String yColumn){
        List<Row> rows = taxiDataService.getDataset()
                .select(xColumn, yColumn)
                .groupBy(xColumn)
                .agg(avg(yColumn))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> avgByFields(String xColumn, String yColumn, List<String> conditions){
        List<Row> rows = taxiDataService.getDataset(conditions)
                .select(xColumn, yColumn)
                .groupBy(xColumn)
                .agg(avg(yColumn))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> avgByFields(String xColumn, String yColumn, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(xColumn, yColumn)
                .groupBy(xColumn)
                .agg(avg(yColumn))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> countByField(String keyColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .groupBy(col(keyColumnName))
                .count()
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> sumByField(String keyColumnName,String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName, valueColumnName)
                .groupBy(keyColumnName)
                .agg(sum(valueColumnName))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> minByField(String keyColumnName,String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName, valueColumnName)
                .groupBy(keyColumnName)
                .agg(min(valueColumnName))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> maxByField(String keyColumnName,String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName, valueColumnName)
                .groupBy(keyColumnName)
                .agg(max(valueColumnName))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> minInColumn(String keyColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) throws Exception {
        try {
            List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                    .select(keyColumnName)
                    .agg(min(keyColumnName))
                    .collectAsList();

            return getAsList(rows);
        }catch (Exception exc){
            return null;
        }
    }

    public List<KeyValueData> maxInColumn(String keyColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName)
                .agg(max(keyColumnName))
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> geoDataForMap(String colLongitude, String colLatitude, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(colLongitude, colLatitude)
                .collectAsList();

        return getAsList(rows);
    }

    public List<KeyValueData> sumOneField(String keyColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) throws Exception {
        try {
            List<Row> rows = taxiDataService.getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                    .select(keyColumnName)
                    .agg(sum(keyColumnName))
                    .collectAsList();

            return getAsList(rows);
        }catch (Exception exc){
            return null;
        }
    }

    public Matrix correlation(String keyColumnName, String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) throws Exception {
        try {
            return Statistics.corr(
                    (taxiDataService
                            .getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                            .toJavaRDD()
                            .map(s ->
                                    Vectors.dense(
                                            Double.parseDouble(
                                                s.getString(s.fieldIndex(keyColumnName)
                                                )
                                            ),
                                            Double.parseDouble(
                                                s.getString(s.fieldIndex(valueColumnName)
                                                )
                                            )
                                    )
                            )
                    )
                            .rdd()
            );
        }catch (Exception exc){
            exc.printStackTrace();
            return null;
        }
    }

    public List<KeyValueData> prediction(String keyColumnName, String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd){
        final double stepSize = 0.002;
        final int numberOfIterations = 10;
        JavaRDD<Row> training = taxiDataService
                .getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName, valueColumnName)
                .toJavaRDD();
        JavaRDD<LabeledPoint> dataLabeledPoints = training.map(mapperFunc);
        LinearRegressionModel model = LinearRegressionWithSGD.train(JavaRDD.toRDD(dataLabeledPoints), numberOfIterations, stepSize);
        JavaRDD<Vector> vectors = dataLabeledPoints.map(LabeledPoint::features);
        JavaRDD<Double> predictions = model.predict(vectors);
        List<KeyValueData> dataList = new ArrayList<>();
        Double counter = 0.0;
        List<Double> list = predictions.take(5);
        for (Double aDouble : list) {
            System.out.println( String.format("%.2f",aDouble));
            dataList.add(new KeyValueData(counter.toString(), String.format("%.2f",aDouble)));
            counter += 1.0;
        }
        return dataList;
    }
}
