package com.myteam.taxidata.counter;

import com.myteam.taxidata.model.KeyValueData;
import com.myteam.taxidata.serv.TaxiDataService;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public abstract class AbstractCounter {
    @Autowired
    TaxiDataService taxiDataService;

    protected List<KeyValueData> getAsList(List<Row> rowsList) {
        rowsList.sort(new Comparator<Row>() {
            @Override
            public int compare(Row o1, Row o2) {
                if (Float.parseFloat(o1.get(0).toString()) > (Float.parseFloat(o2.get(0).toString())))
                    return 1;
                else if (Float.parseFloat(o1.get(0).toString()) < (Float.parseFloat(o2.get(0).toString())))
                    return -1;
                else
                    return 0;
            }
        });

        return rowsList.stream()
                .map(new java.util.function.Function<Row, KeyValueData>() {
                    @Override
                    public KeyValueData apply(Row row) {
                        return new KeyValueData(row);
                    }
                })
                .collect(Collectors.toList());
    }
}
