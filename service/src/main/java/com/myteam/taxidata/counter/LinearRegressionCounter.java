package com.myteam.taxidata.counter;

import scala.collection.JavaConversions;
import scala.collection.JavaConversions.*;
import com.myteam.taxidata.model.KeyValueData;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.regression.LinearRegressionWithSGD;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Row;
import org.springframework.stereotype.Component;
import scala.collection.Seq;

import java.util.*;

import static com.myteam.taxidata.counter.TaxiCounter.mapperFunc;

@Component
public class LinearRegressionCounter extends AbstractCounter{
    private final double stepSize = 0.002;
    private final int numberOfIterations = 10;

    public Double linearRegressionPrediction(String keyColumnName, String valueColumnName, String keyColumnData, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd){
        JavaRDD<Row> training = taxiDataService
                .getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(keyColumnName, valueColumnName, "Passenger_count")
                .toJavaRDD();
        JavaRDD<LabeledPoint> dataLabeledPoints = training.map(mapperFunc);
        dataLabeledPoints.cache().collect();
        LinearRegressionModel model = LinearRegressionWithSGD.train(JavaRDD.toRDD(dataLabeledPoints), numberOfIterations, stepSize);
        JavaRDD<Vector> vectors = dataLabeledPoints.map(LabeledPoint::features);
        return model.predict(Vectors.dense(Double.parseDouble(keyColumnData), 4.0));
        //return model.predictPoint(vectors.first(), model.weights(), Integer.parseInt(keyColumnData));
    }

    public Double linearRegressionPrediction(String keys, ArrayList<String> values, String valueColumnName, List<String> conditions, String sourceType, String sourceDateStart, String sourceDateEnd) {
        ArrayList <String> list = new ArrayList<>(Arrays.asList(keys.split(",")));
        Seq<String> name = JavaConversions.asScalaBuffer(list);
        JavaRDD<Row> training = taxiDataService
                .getDataset(conditions, sourceType, sourceDateStart, sourceDateEnd)
                .select(valueColumnName, name)
                .toJavaRDD();

        JavaRDD<LabeledPoint> dataLabeledPoints = training.map(mapperFunc);
        dataLabeledPoints.cache().collect();
        double featureCollection[] = values.stream().mapToDouble(Double::parseDouble).toArray();
        LinearRegressionModel model = LinearRegressionWithSGD.train(JavaRDD.toRDD(dataLabeledPoints), numberOfIterations, stepSize);
        return model.predict(Vectors.dense(featureCollection));
    }
}
