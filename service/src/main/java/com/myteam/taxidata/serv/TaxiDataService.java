package com.myteam.taxidata.serv;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

@Component
public class TaxiDataService extends AbstractDataService{

    public Dataset<Row> getDataset(){
        return sparkSession.read().format("csv")
                .option("header","true")
                .load("input/green_tripdata_2016-01.csv");
    }

    public Dataset<Row> getDataset(String type, String month, String year){
        return sparkSession.read().format("csv")
                .option("header","true")
                .load("input/"+type+"_tripdata_20"+year+"-"+month+".csv");
    }

    public Dataset<Row> getDataset(List<String> conditions) {
        Dataset<Row> unfilteredSet = getDataset();
        for (String condition:conditions
                ) {
            unfilteredSet = unfilteredSet.filter(condition);
        }
        return unfilteredSet;
    }

    public Dataset<Row> getDataset(List<String> conditions, int month, int year, String type) {
        NumberFormat formatter = new DecimalFormat("00");
        String monthLine = formatter.format(month);
        String yearLine = formatter.format(year);
        Dataset<Row> unfilteredSet = getDataset(type, monthLine, yearLine);
        for (String condition:conditions
                ) {
            unfilteredSet = unfilteredSet.filter(condition);
        }
        return unfilteredSet;
    }



    public Dataset<Row> getDataset(List<String> conditions, String sourceTypes, String sourceDateStart, String sourceDateEnd) {
        Dataset<Row> resultingSet;
        boolean isGreen = false;
        boolean isYellow = false;
        int startYear, startMonth, finishYear, finishMonth;
        startYear = Integer.parseInt(sourceDateStart.substring(2,4));
        finishYear = Integer.parseInt(sourceDateEnd.substring(2,4));
        startMonth = Integer.parseInt(sourceDateStart.substring(0,2));
        finishMonth = Integer.parseInt(sourceDateEnd.substring(0,2));
        if (sourceTypes.contains("g")&& (sourceTypes.contains("y"))) {
            resultingSet = getDataset(conditions, startMonth, startYear,"green");
            Dataset<Row> addSet =   getDataset(conditions, startMonth, startYear,"yellow");

            resultingSet = resultingSet
                    .drop("ehail_fee")
                    .drop("Trip_type ")
                    .unionAll(addSet)
                    .distinct();
        }
        else if (sourceTypes.contains("g")) {
            isGreen = true;
            resultingSet = getDataset(conditions,startMonth, startYear,"green");
        }
        else if (sourceTypes.contains("y")) {
            isYellow = true;
            resultingSet = getDataset(conditions,startMonth, startYear,"yellow");
        }
        else
            return null;
        for (int yearCount = startYear; yearCount < finishYear; yearCount++){
            for (int monthCount = startMonth+1; monthCount < finishMonth; monthCount++){
                if (isGreen) {
                    resultingSet = resultingSet
                            .drop("ehail_fee")
                            .drop("Trip_type ")
                            .unionAll(getDataset(conditions, monthCount, yearCount, "green")
                                                .drop("ehail_fee")
                                                    .drop("Trip_type ")
                            )
                            .distinct();
                    resultingSet.union(
                            this.getDataset(conditions, monthCount, yearCount, "green")
                    );
                }
                if (isYellow)
                    resultingSet.drop("ehail_fee")
                        .drop("Trip_type ").unionAll(
                        this.getDataset(conditions, monthCount, yearCount,"yellow")).distinct();
            }
        }
        return resultingSet.filter("Total_amount>0")
                .filter("Total_amount<100")
                .filter("Trip_distance>0")
                .filter("Passenger_count>0");
    }
}
