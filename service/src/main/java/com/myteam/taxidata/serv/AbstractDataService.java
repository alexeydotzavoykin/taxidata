package com.myteam.taxidata.serv;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDataService {
    @Autowired
    protected SparkSession sparkSession;

    public abstract Dataset<Row> getDataset();
}
