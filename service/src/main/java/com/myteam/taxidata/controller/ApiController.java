package com.myteam.taxidata.controller;

import com.myteam.taxidata.counter.LinearRegressionCounter;
import com.myteam.taxidata.counter.TaxiCounter;
import com.myteam.taxidata.model.KeyValueData;
import org.apache.spark.mllib.linalg.Matrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RequestMapping("/api")
@RestController
public class ApiController {
    @Autowired
    TaxiCounter taxiCounter;

    @Autowired
    LinearRegressionCounter linearRegressionCounter;

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/avg/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_avg_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                @PathVariable("columnName2") String valueColumnName,
                                                                @PathVariable("conditions") List<String> conditions,
                                                                @PathVariable("sourceType") String sourceType,
                                                                @PathVariable("sourceDateStart") String sourceDateStart,
                                                                @PathVariable("sourceDateEnd") String sourceDateEnd
                                                                ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/avg/Payment_type-Total_amount
                Reading data for Green taxi from Jan, 2016 to Jan, 16 (one month * one type = 1 file)
                Conditions are: Trip_distance>0,RateCodeID=1,Trip_distance<1
                Counts: average total amount for every payment type on trips given
         */
        return new ResponseEntity<>(taxiCounter.avgByFields(keyColumnName, valueColumnName, conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/geo/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_map_geo_rate_clients(@PathVariable("columnName1") String colLongitude,
                                                                @PathVariable("columnName2") String colLatitude,
                                                                @PathVariable("conditions") List<String> conditions,
                                                                @PathVariable("sourceType") String sourceType,
                                                                @PathVariable("sourceDateStart") String sourceDateStart,
                                                                @PathVariable("sourceDateEnd") String sourceDateEnd ) {
        /*
            sample http://localhost:8080/api/taxi/source-y:0116-0116/filter:Store_and_fwd_flag=Y/Dropoff_longitude-Dropoff_latitude/geo

            http://localhost:8080/api/taxi/source-y:0116-0116/filter:Store_and_fwd_flag=Y,VendorID=2/Dropoff_longitude-Dropoff_latitude/geo
         */
        return new ResponseEntity<>(taxiCounter.geoDataForMap(colLongitude,colLatitude, conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/count/{columnName1}")
    public ResponseEntity<List<KeyValueData>> taxi_count_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                @PathVariable("conditions") List<String> conditions,
                                                                @PathVariable("sourceType") String sourceType,
                                                                @PathVariable("sourceDateStart") String sourceDateStart,
                                                                @PathVariable("sourceDateEnd") String sourceDateEnd
    ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.countByField(keyColumnName, conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/sumByField/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_sum_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                @PathVariable("columnName2") String valueColumnName,
                                                                  @PathVariable("conditions") List<String> conditions,
                                                                  @PathVariable("sourceType") String sourceType,
                                                                  @PathVariable("sourceDateStart") String sourceDateStart,
                                                                  @PathVariable("sourceDateEnd") String sourceDateEnd
    ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.sumByField(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/minByField/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_minByField_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                @PathVariable("columnName2") String valueColumnName,
                                                                @PathVariable("conditions") List<String> conditions,
                                                                @PathVariable("sourceType") String sourceType,
                                                                @PathVariable("sourceDateStart") String sourceDateStart,
                                                                @PathVariable("sourceDateEnd") String sourceDateEnd
    ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.minByField(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/maxByField/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_maxByField_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                       @PathVariable("columnName2") String valueColumnName,
                                                                       @PathVariable("conditions") List<String> conditions,
                                                                       @PathVariable("sourceType") String sourceType,
                                                                       @PathVariable("sourceDateStart") String sourceDateStart,
                                                                       @PathVariable("sourceDateEnd") String sourceDateEnd
    ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.maxByField(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/max/{columnName1}")
    public ResponseEntity<List<KeyValueData>> taxi_maxInColumn_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                       @PathVariable("conditions") List<String> conditions,
                                                                       @PathVariable("sourceType") String sourceType,
                                                                       @PathVariable("sourceDateStart") String sourceDateStart,
                                                                       @PathVariable("sourceDateEnd") String sourceDateEnd
    ) {
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.maxInColumn(keyColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/min/{columnName1}")
    public ResponseEntity<List<KeyValueData>> taxi_minInColumn_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                        @PathVariable("conditions") List<String> conditions,
                                                                        @PathVariable("sourceType") String sourceType,
                                                                        @PathVariable("sourceDateStart") String sourceDateStart,
                                                                        @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        /* sample /taxi/source-g:0116-0116/filter:Trip_distance>0,RateCodeID=1,Trip_distance<1/count/Payment_type

                Counts: amount of usage of each payment type
         */
        return new ResponseEntity<>(taxiCounter.minInColumn(keyColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/sum/{columnName1}")
    public ResponseEntity<List<KeyValueData>> taxi_sum_filtered(@PathVariable("columnName1") String keyColumnName,
                                                                        @PathVariable("conditions") List<String> conditions,
                                                                        @PathVariable("sourceType") String sourceType,
                                                                        @PathVariable("sourceDateStart") String sourceDateStart,
                                                                        @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        /* sample

                Counts:
         */
        return new ResponseEntity<>(taxiCounter.sumOneField(keyColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd), HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/correlation/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_correlation(@PathVariable("columnName1") String keyColumnName,
                                                   @PathVariable("columnName2") String valueColumnName,
                                                   @PathVariable("conditions") List<String> conditions,
                                                   @PathVariable("sourceType") String sourceType,
                                                   @PathVariable("sourceDateStart") String sourceDateStart,
                                                   @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        Matrix matrix = taxiCounter.correlation(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd);
        List<KeyValueData> list = new ArrayList<>();
        double matrixArray[] = matrix.toArray();
        for (int i=0; i< matrixArray.length; i++){
            list.add(new KeyValueData(String.valueOf(i),String.valueOf(Math.abs(matrixArray[i]))));
        }

        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/linearRegression/{columnName1}-{columnName2}")
    public ResponseEntity<List<KeyValueData>> taxi_linearRegression(@PathVariable("columnName1") String keyColumnName,
                                                               @PathVariable("columnName2") String valueColumnName,
                                                               @PathVariable("conditions") List<String> conditions,
                                                               @PathVariable("sourceType") String sourceType,
                                                               @PathVariable("sourceDateStart") String sourceDateStart,
                                                               @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        //taxiCounter.prediction(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd);

        return new ResponseEntity<>(taxiCounter.prediction(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd),HttpStatus.OK);

    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/linearRegression/{columnName1}-{columnName2}/{keyColumnValue}")
    public ResponseEntity<List<KeyValueData>> taxi_linearRegressionAdvanced(@PathVariable("columnName1") String keyColumnName,
                                                                    @PathVariable("columnName2") String valueColumnName,
                                                                    @PathVariable("keyColumnValue") String keyColumnValue,
                                                                    @PathVariable("conditions") List<String> conditions,
                                                                    @PathVariable("sourceType") String sourceType,
                                                                    @PathVariable("sourceDateStart") String sourceDateStart,
                                                                    @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        //taxiCounter.prediction(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd);

        Map<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(keyColumnName, keyColumnValue);
        System.out.println(linearRegressionCounter.linearRegressionPrediction(keyColumnName, valueColumnName, keyColumnValue, conditions, sourceType, sourceDateStart, sourceDateEnd));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping("/taxi/source-{sourceType}:{sourceDateStart}-{sourceDateEnd}/filter:{conditions}/linearRegression/columns:{columns}/values:{keyColumnValues}/{finalColumn}")
    public ResponseEntity<List<KeyValueData>> taxi_linearRegressionAdvancedMap(@PathVariable("columns") String columns,
                                                                            @PathVariable("keyColumnValues") String keyColumnValues,
                                                                            @PathVariable("finalColumn") String finalColumn,
                                                                            @PathVariable("conditions") List<String> conditions,
                                                                            @PathVariable("sourceType") String sourceType,
                                                                            @PathVariable("sourceDateStart") String sourceDateStart,
                                                                            @PathVariable("sourceDateEnd") String sourceDateEnd
    ) throws Exception{
        //taxiCounter.prediction(keyColumnName, valueColumnName,conditions, sourceType, sourceDateStart, sourceDateEnd);
        String[] values = keyColumnValues.split(",");
        //System.out.println(linearRegressionCounter.linearRegressionPrediction(columns, new ArrayList<>(Arrays.asList(values)), finalColumn, conditions, sourceType, sourceDateStart, sourceDateEnd));
        Double answer = linearRegressionCounter.linearRegressionPrediction(columns, new ArrayList<>(Arrays.asList(values)), finalColumn, conditions, sourceType, sourceDateStart, sourceDateEnd);
        List<KeyValueData> list = new ArrayList<>();
        list.add(new KeyValueData("1", answer.toString()));
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
}
