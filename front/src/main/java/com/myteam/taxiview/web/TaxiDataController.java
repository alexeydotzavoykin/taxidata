package com.myteam.taxiview.web;

import com.myteam.taxiview.model.KeyValueData;
import com.myteam.taxiview.model.QueryOptions;
import com.myteam.taxiview.service.SparkService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
@RequestMapping("")
public class TaxiDataController {
    @Autowired
    SparkService sparkService;

    ArrayList<List<KeyValueData>> allDataResultSets = new ArrayList<>();

    @ModelAttribute("allMonths")
    public String[] allMonthsList() {
        return new String[]{"01", "02", "03", "04", "05", "06", "07", "06", "09", "10", "11", "12"};
    }

    @ModelAttribute("allYears")
    public String[] allYearsList() { /* TODO: Replace with Arr.From (Range(9, 17)).map(x => x.toString(x, 2)) */
        return new String[]{"17", "16", "09", "10", "11", "12", "13", "14", "15"};
    }

    @ModelAttribute("allOperations")
    public String[] allOperationsList() {
        return new HashSet<String> (Arrays.AsList("avg", "count", "sum", "min", "max", "minByField", "maxByField", "sumByField", "correlation", "linearRegression"));
    }

    @ModelAttribute("allDotTypes")
    public String[] allDotTypes() {
        return new HashSet<String> (Arrays.AsList("Посадка", "Высадка"));
    }

    @ModelAttribute("allResults")
    public ArrayList<List<KeyValueData>> allDataResultSets(){
        return allDataResultSets;
    }

    @ModelAttribute("allSources")
    public String[] allSourcesList() {
        return new HashSet<String> (Arrays.AsList("g", "yg",  "y"));
    }

    @RequestMapping(value = "taxiapp", params = {"operation","conditions","columns","startMonth","startYear",
            "endMonth", "endYear" , "sources"})
    public String singleCallWith (HttpServletRequest req, Model model, QueryOptions options) throws Exception
    {
        String call = "source-" + options.getSources() + ":" +
                options.getStartMonth() + options.getStartYear() + "-" +
                options.getEndMonth() + options.getEndYear() + "/" +
                "filter" + ":" + options.getConditions()  + "/" +
                options.getOperation()  + "/" +
                options.getColumns()  + "/";
        allDataResultSets.add(sparkService.readInfo(call));
        model.addAttribute("options", options);
        //model.addAttribute("reqs", new QueryOptions());
        return "mainpage";
    }

    @RequestMapping(value = "")
    public String singleLanding () throws Exception
    {
        return "landingPage";
    }

    @RequestMapping(value = "correlations", params = {"conditions","columns","startMonth","startYear",
            "endMonth", "endYear" , "sources"})
    public String singleCallWithCorrelation (HttpServletRequest req, Model model, QueryOptions options) throws Exception
    {
        String call = "source-" + options.getSources() + ":" +
                options.getStartMonth() + options.getStartYear() + "-" +
                options.getEndMonth() + options.getEndYear() + "/" +
                "filter" + ":" + options.getConditions()  + "/" +
                "correlation"  + "/" +
                options.getColumns()  + "/";
        allDataResultSets.add(sparkService.readInfo(call));
        model.addAttribute("correlationLevel", allDataResultSets.get(allDataResultSets.size()-1).get(2).getValue());
        model.addAttribute("options", options);
        return "correlationPage";
    }

    @RequestMapping(value = "mapOperations", params = {"conditions","dotType","startMonth","startYear",
            "endMonth", "endYear" , "sources", "crimeType"})
    public String singleCallWithMap (HttpServletRequest req, Model model, QueryOptions options) throws Exception
    {
        String columnNames = "";
        if (Objects.equals(options.getDotType(), "Посадка"))
            columnNames = "Pickup_longitude-Pickup_latitude";
        else if (Objects.equals(options.getDotType(), "Высадка"))
            columnNames = "Dropoff_longitude-Dropoff_latitude";
        String call = "source-" + options.getSources() + ":" +
                options.getStartMonth() + options.getStartYear() + "-" +
                options.getEndMonth() + options.getEndYear() + "/" +
                "filter" + ":" + options.getConditions()  + "/" +
                "geo"  + "/" +
                columnNames  + "/";
        allDataResultSets.add(sparkService.readInfo(call));
        model.addAttribute("options", options);
        model.addAttribute("allCrimes", sparkService.readCrime(options.getCrimeType()));
        return "mapPage";
    }

    @RequestMapping(value = "taxiapp")
    public String singleFirstPage(HttpServletRequest req, Model model)
    {
        model.addAttribute("options", new QueryOptions());
        return "mainpage";
    }

    @RequestMapping(value = "linearRegression")
    public String linRegPage(Model model) {
        model.addAttribute("options", new QueryOptions());
        return "linearRegressionPage";
    }

    @RequestMapping(value = "mapOperations")
    public String mapPage(Model model) {
        model.addAttribute("options", new QueryOptions());
        return "mapPage";
    }

    @RequestMapping(value = "help")
    public String helpPage(Model model) {
        return "helpPage";
    }

    @RequestMapping(value = "correlations")
    public String correlationPage(Model model) {
        model.addAttribute("options", new QueryOptions());
        model.addAttribute("correlationLevel", "0");
        return "correlationPage";
    }

    @RequestMapping(value = "taxiapp", params = "delData")
    public String singleFirstPageDeletion(HttpServletRequest req, Model model, QueryOptions options)
    {
        allDataResultSets = new ArrayList<>();
        model.addAttribute("options", options);
        return "mainpage";
    }

    @RequestMapping(value = "linearRegression", params = {"conditions","columns","startMonth","startYear",
            "endMonth", "endYear" , "sources", "values", "predictionColumn"})
    public String singleCallWitLinearRegression (HttpServletRequest req, Model model, QueryOptions options) throws Exception
    {
        String call = "source-" + options.getSources() + ":" +
                options.getStartMonth() + options.getStartYear() + "-" +
                options.getEndMonth() + options.getEndYear() + "/" +
                "filter" + ":" + options.getConditions()  + "/" +
                "linearRegression"  + "/" + "columns:" +
                options.getColumns()  + "/" + "values:" +
                options.getValues()  + "/" + options.getPredictionColumn();
        List<KeyValueData> answer = sparkService.readInfo(call);
        allDataResultSets.add(answer);
        model.addAttribute("regressionValue", answer.get(0).getValue());
        model.addAttribute("options", options);
        return "linearRegressionPage";
    }
}