package com.myteam.taxiview.service;

import com.myteam.taxiview.model.KeyValueData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SparkService {
    private RestTemplate rest = new RestTemplate();

    private final String CITY_OF_NEW_YOUR_SITE_BASE_URL =
            "https://data.cityofnewyork.us/resource/7x9x-zpz6.json?ofns_desc=";
    private final TAXI_API_ENDPOINT = "api/taxi/";

    @Value("${sparkJobsServerPort}")
    String sparkServiceAddress;

    /*Specs Example : +"source-g:0116-0116
                            /filter:Trip_distance>0,RateCodeID=1,Trip_distance<1
                            /avg
                            /Payment_type-Total_amount", */
    public List<KeyValueData> readInfo(String specs) throws Exception{

        KeyValueData[] data = new KeyValueData[]{};
        try {
            data = rest.getForObject(sparkServiceAddress + TAXI_API_ENDPOINT
                            + specs,
                    KeyValueData[].class);
        }catch (Exception exception){
            return Arrays.asList(data).stream().map(x -> {
                return x;
            }).collect(Collectors.toList());
        }
        if (data == null)
            return Arrays.asList(new KeyValueData[]{}).stream().map(x -> {
                return x;
            }).collect(Collectors.toList());
        else
            return Arrays.asList(data).stream().map(x -> {
                return x;
            }).collect(Collectors.toList());
    }

    public ArrayList<KeyValueData> readCrime(String crimeType) {
        Object[] data;
        ArrayList<KeyValueData> result = new ArrayList<>();
        try {
            data = rest.getForObject(CITY_OF_NEW_YOUR_SITE_BASE_URL + crimeType,
                    Object[].class);
        }catch (Exception exception){
            return null;
        }
        for (Object aData : data) {
            result.add(new KeyValueData(((LinkedHashMap) aData).getOrDefault("longitude","0").toString(),
                    ((LinkedHashMap) aData).getOrDefault("latitude","0").toString()));
        }
        return result;
    }
}
