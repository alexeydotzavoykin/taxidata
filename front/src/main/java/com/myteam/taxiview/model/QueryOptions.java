package com.myteam.taxiview.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryOptions {
    String operation;
    String conditions;
    String columns;
    String startMonth;
    String startYear;
    String endMonth;
    String endYear;
    String sources;
    String crimeType;
    String dotType;
    String values;
    String predictionColumn;

    public QueryOptions(String operation, String conditions, String columns, String startMonth, String startYear, String endMonth, String endYear, String sources, String crimeType, String dotType, String values, String predictionColumn) {
        this.operation = operation;
        this.conditions = conditions;
        this.columns = columns;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.endMonth = endMonth;
        this.endYear = endYear;
        this.sources = sources;
        this.crimeType = crimeType;
        this.dotType = dotType;
        this.values = values;
        this.predictionColumn = predictionColumn;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public String getPredictionColumn() {
        return predictionColumn;
    }

    public void setPredictionColumn(String predictionColumn) {
        this.predictionColumn = predictionColumn;
    }

    public QueryOptions() {
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getCrimeType() {
        return crimeType;
    }

    public void setCrimeType(String crimeType) {
        this.crimeType = crimeType;
    }

    public QueryOptions(String operation, String conditions, String columns, String startMonth, String startYear, String endMonth, String endYear, String sources, String crimeType) {
        this.operation = operation;
        this.conditions = conditions;
        this.columns = columns;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.endMonth = endMonth;
        this.endYear = endYear;
        this.sources = sources;
        this.crimeType = crimeType;
    }

    public QueryOptions(String operation, String conditions, String columns, String startMonth, String startYear, String endMonth, String endYear, String sources, String crimeType, String dotType) {
        this.operation = operation;
        this.conditions = conditions;
        this.startMonth = startMonth;
        this.startYear = startYear;
        this.endMonth = endMonth;
        this.endYear = endYear;
        this.sources = sources;
        this.crimeType = crimeType;
        this.dotType = dotType;
    }

    public String getDotType() {
        return dotType;
    }

    public void setDotType(String dotType) {
        this.dotType = dotType;
    }
}
