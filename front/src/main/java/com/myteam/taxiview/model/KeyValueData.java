package com.myteam.taxiview.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyValueData {

    public KeyValueData(){
        this.key =  "";
        this.value =  "";
    }

    private String key;
    private String value;

    public KeyValueData(String key, String value) {
        this.key =  key;
        this.value =  value;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
